#include <cassert>

#include "fe_2d.h"

FE2D::FE2D(const Matrice& coord, const Matrice& D,
           const Matrice& F, const Matrice& Mcoef):
    coord_(coord),
    D_(D),
    F_(F),
    Mcoef_(Mcoef)
{}

double FE2D::N(double xi,double eta,int lnn)
{
    assert(lnn >= 1 && lnn <= 4);

    switch(lnn) {
    case 1:
        return .25*(1-xi)*(1-eta);
    case 2:
        return .25*(1+xi)*(1-eta);
    case 3:
        return .25*(1+xi)*(1+eta);
    case 4:
        return .25*(1-xi)*(1+eta);
    }
}

double FE2D::DNxi(double xi,double eta,int lnn)
{
    assert(lnn >= 1 && lnn <= 4);

    switch(lnn) {
    case 1:
        return -.25*(1-eta) /* + 0*xi */;
    case 2:
        return .25*(1-eta) /* + 0*xi */;
    case 3:
        return .25*(1+eta) /* + 0*xi */;
    case 4:
        return -.25*(1+eta) /* + 0*xi */;
    }

}

double FE2D::DNeta(double xi,double eta,int lnn)
{
    assert(lnn >= 1 && lnn <= 4);

    switch(lnn) {
    case 1:
        return -.25*(1-xi) /* + 0*eta */;
    case 2:
        return -.25*(1+xi) /* + 0*eta */;
    case 3:
        return .25*(1+xi) /* + 0*eta */;
    case 4:
        return .25*(1-xi) /* + 0*eta */;
    }

}

double FE2D::detJac(double xi,double eta)
{
    double result = 0.0;
    for(int i=1; i<=4; ++i) {
        for(int j=1; j<=4; ++j) {
            result += coord_(i,1) * DNxi(xi,eta,i)
                       * coord_(j,2) * DNeta(xi,eta,j)
                   - coord_(i,1) * DNeta(xi,eta,i)
                       * coord_(j,2) * DNxi(xi,eta,j);
        }
    }
    return result;
}

double FE2D::Grad(double xi, double eta, int lnn, int n)
{
    assert(n == 1 || n ==2);

    if(n == 1) {
    //the x derivative
        double xcoef = 0.0;
        for(int j=1; j<=4; j++) {
            xcoef += coord_(j,2)*DNeta(xi,eta,j)*DNxi(xi,eta,lnn)
                     - coord_(j,2)*DNxi(xi,eta,j)*DNeta(xi,eta,lnn);
        }
        return xcoef / detJac(xi, eta);
    } else { // if (n == 2)
    //the y derivative
        double ycoef = 0.0;
        for(int i=1; i<=4; i++) {
            ycoef += coord_(i,1)*DNxi(xi,eta,i)*DNeta(xi,eta,lnn)
                     - coord_(i,1)*DNeta(xi,eta,i)*DNxi(xi,eta,lnn);
        }
        return ycoef / detJac(xi, eta);
    }
}


double FE2D::Kfun(double xi,double eta,int i,int j)
{
    // elenent (i,j) of elementary matrix K.
    double kij = 0.0;
    for(int m=1; m<=2; m++) {
        for(int n=1; n<=2; n++) {
            kij += Grad(xi,eta,i,m)*D_(m,n)*Grad(xi,eta,j,n)*detJac(xi,eta);
        }
    }
    return kij;
}

Matrice FE2D::Kel()
{
    return MatGausQuad(&FE2D::Kfun);
}

double FE2D::Mfun(double xi,double eta,int i,int j)
{
    double mcoef = 0.0;
    for(int m=1; m<=4; m++)
    {
        //interpolate of the function that mutliplies the mass matrix
        mcoef += Mcoef_(m)*N(xi,eta,m);
    }
    return mcoef*N(xi,eta,i)*N(xi,eta,j)*detJac(xi,eta);
}

Matrice FE2D::Mel()
{
    return MatGausQuad(&FE2D::Mfun);
}

double FE2D::Ffun(double xi,double eta,int i)
{
    double f = 0.0;
    for(int m=1; m<=4; m++)
    {
        f += F_(m)*N(xi,eta,m);//interpolate the source function
    }

    return f*N(xi,eta,i)*detJac(xi,eta);
}


Matrice FE2D::Fel()
{
    return VectGausQuad(&FE2D::Ffun);
}

Matrice FE2D::MatGausQuad(FE2D::MGQFunc func)
{
    // Elementary matrix.
    Matrice mat_el(4,4);

    for(int i=1; i<=4; i++) {
        for(int j=1; j<=4; j++) {
            mat_el(i,j) = (this->*func)(-gq_2_p,-gq_2_p,i,j)
                          + (this->*func)(-gq_2_p,gq_2_p,i,j)
                          + (this->*func)(gq_2_p,-gq_2_p,i,j)
                          + (this->*func)(gq_2_p,gq_2_p,i,j);
        }
    }
    return mat_el;
}

Matrice FE2D::VectGausQuad(FE2D::VGQFunc func)
{
    Matrice vect_el(4,1);
    for(int i=1; i<=4; i++) {
        vect_el(i) = (this->*func)(-gq_2_p,-gq_2_p,i)
                     + (this->*func)(-gq_2_p,gq_2_p,i)
                     + (this->*func)(gq_2_p,-gq_2_p,i)
                     + (this->*func)(gq_2_p,gq_2_p,i);
    }
    return vect_el;
}
