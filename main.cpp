/**
 *  The 2D FEM solver.
 *  Can solve the general scalar problem of the form:
 *  grad(-D(x,y)*grad(U(x,y))) +c(x,y)*U(x,y) = f(x,y) where:
 *  D is a second order tensor (depends analytically on x and y)
 *  c and f are the scalar functions (depend analytically on x and y)
 *  For the moment it solves the problem when grad(U) = 0 on Neumann boundaries.
 *
 *  Geometrical information is extracting from a mesh file (of gmsh) which
 *  contains nodes, 4-node quadrangles of elements and segments between all the
 *  boundary nodes.
 *  Format of the geometry file must be as the following:
 *  $MeshFormat
 *  2.2 0 8 $ unused
 *  $EndMeshFormat
 *  $Nodes
 *  121 $ number_of_nodes
 *  14 8 0.4 0 $ node_number x y unused_z
 *  $ ...
 *  $EndNodes
 *  $Elements
 *  121 $ number_of_elements
 *  4 1 2 1 1 3 4 $ 4=bc_segment_number 1=segment_type
 *                $ number_of_unused unused_phys_entity unused_geom_entity ...
 *                $ 3=node1_number 4=node2_number
 *  $...
 *  9 3 2 2 1 1 2 8 9 $ 9=element_number 3=quadrangle_element_type
 *                    $ number_of_unused unused_phys_entity unused_geom_entity .
 *                    $ 1=node1_number 2=node2_number
 *                    $ 8=node3_number 9=node4_number
 *  $...
 *  $EndElements
 *  $...
 *
 *  Format of the physical properties file must be as the following:
 *  $value of u on the boundary nodes:
 *  46.0                                    $ + x + y
 *  $term f:
 *  .00108                                  $ + sin(x) - exp(y)
 *  $term c:
 *  .000054                                 $ + sqrt(avg(x, y, x)^2 + sum(x, y))
 *  $ term d11 of tensor D
 *  .164                                    $ + x * y
 *  $ term d12 of tensor D
 *  .0                                      $ + min(x,y)
 *  $ term d21 of tensor D
 *  .0                                      $ + (x > y) ? 1 : 2
 *  $ term d22 of tensor D
 *  .164                                    $ + x && y
 */

#include <iostream>
#include "matrice.h"
#include "mesh_2d.h"
#include "assemblage.h"
#include "solution.h"

int main(int argc, char* argv[])
{
    if (argc < 4) {
        std::cerr << "Usage: " << argv[0]
            << " mesh_input phys_props_input solution_output" << std::endl;
        return 1;
    }

    Mesh2D mesh(argv[1], argv[2]);
    Assemblage global(mesh.GCM(), mesh.IEN(), mesh.ID(), mesh.D());
    Matrice K = global.K();
    Matrice M = global.M();
    Matrice F = global.F();
    Matrice K_res = K + M;
    // Resultant values for non-bc nodes.
    Matrice SLE_solution = gauss_seidel(K_res, F, 1.85);

    Solution solution(mesh.ID(), mesh.GCM(), SLE_solution);
    solution.to_file(argv[3]);
    std::cout << "x\ty\tsolution" << std::endl << solution.get();

    return 0;
}
