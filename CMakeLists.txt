project(fem)
cmake_minimum_required(VERSION 2.8)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/")

find_package(muParser REQUIRED)
include_directories(${MUPARSER_INCLUDE_DIRS})

add_executable(fem assemblage.cpp fe_2d.cpp matrice.cpp solution.cpp
               mesh_2d.cpp main.cpp)
target_link_libraries(fem ${MUPARSER_LIBRARIES})
