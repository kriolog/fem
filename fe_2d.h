#ifndef FE_2D
#define FE_2D

//code specifique pour notre probleme

#include "matrice.h"

/// 2D finite element
class FE2D
{
public:
    // Gauss quadrature 2 point.
    static const double gq_2_p = .57735026918962;

    /// coord= |x1 y1|      D= |d11 d12|
    ///        |x2 y2|         |d21 d22|
    ///        |x3 y3|
    ///        |x4 y4|
    ///
    ///     F=|f1|      Mcoef=|m1|
    ///       |f2|            |m2|
    ///       |f3|            |m3|
    ///       |f4|            |m4|
    ///
    /// Coord: matrice des coordonnees de l'element 2D quadrilaterale
    /// D: tenseur de diffusion ou conductivite thermique selon probleme
    /// F: vecteur qui contient les valeurs(a chaque noeud de l'element) du terme de source
    /// Mcoef:vecteur que contient les valeurs de la fonction qui multiplie le terme potentiel
    ///
    /// l'equation differentielle est: D.T" + Mcoef.T= F
    FE2D(const Matrice& coord, const Matrice& D,
         const Matrice& F, const Matrice& Mcoef);
    Matrice Kel();
    Matrice Mel();
    Matrice Fel();
    void afficheCoords();

private:
    // Function for matrix Gauss quadrature
    typedef double (FE2D::*MGQFunc)(double,double,int,int);
    // Function for vector Gauss quadrature
    typedef double (FE2D::*VGQFunc)(double,double,int);

    Matrice coord_;
    Matrice D_;
    Matrice F_;
    Matrice Mcoef_;
    //N: fonction d'interpolation de Langrange
    double N(double xi,double eta,int lnn);//lnn:local node number, 1<=N<=4
    //DNxi derivee partielle de N par rapport a xi
    double DNxi(double xi,double eta,int lnn);//
    //DNeta derivee partielle de N par rapport a eta
    double DNeta(double xi,double eta,int lnn);
    //Grad: gradient
    double Grad(double xi,double eta,int lnn,int n);//n=1 for the x derivative,
                                                 //n=2 for the y derivative
    //detJac: determinant de la matrice de Jacobi
    double detJac(double xi,double eta);//
    // Kfun: K function. C'est tout le terme dans l'integrale de la matrice Kij
    // (matrice de rigidite)
    double Kfun(double xi,double eta,int i, int j);
    // Mfun: M function. C'est tout le terme dans l'integrale de la matrice Mij
    // (matrice de masse)
    double Mfun(double xi,double eta,int i, int j);
    // Ffun: M function. C'est tout le terme dans l'integrale du vecteur du
    // terme de source (F comme force)
    double Ffun(double xi,double eta,int i);//for the rhs of the problem

    //integration par quadrature de Gauss a deux points pour une matrice
    Matrice MatGausQuad(MGQFunc func); //matrix gaussian quadrature
    //integration par quadrature de Gauss a deux points pour un vecteur
    Matrice VectGausQuad(VGQFunc func); //vector gaussian quadrature
};
#endif // FE_2D
