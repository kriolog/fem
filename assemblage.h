#ifndef ASSEMBLAGE_H
#define ASSEMBLAGE_H

#include "matrice.h"

class Assemblage
{
public:
    // D is the list of the diffision tensor for all the elenments. Each tensor
    // is stocked as one line (d11, d12, d21, d22).
    Assemblage(const Matrice &GCM, const Matrice &IEN,
               const Matrice &ID, const Matrice &D);
    // D is the same as for constructor.
    void assemble(const Matrice &GCM, const Matrice &IEN,
                  const Matrice &ID, const Matrice &D);
    const Matrice& K() { return K_; }
    const Matrice& M() { return M_; }
    const Matrice& F() { return F_; }

private:
    // number of elements
    int nel_;
    // number of equations
    int neq_;
    // number of nodes
    int nnd_;
    Matrice K_;
    Matrice M_;
    Matrice F_;

    // number of equations
    int neq(const Matrice& ID);
};

#endif // ASSEMBLAGE_H
