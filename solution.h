#ifndef SOLUTION_H
#define SOLUTION_H

#include <string>

#include "matrice.h"

class Solution
{
public:
    Solution(const Matrice& ID, const Matrice& GcM, const Matrice& non_bc_sol);
    const Matrice& get() const { return solution_; }
    void to_file(const std::string& sol_file_name) const;

private:
    Matrice solution_;

    /// @param non_bc_sol - Solution fo unknown (not of boundary condition)
    /// nodes. Numeration correspond to equations.
    void assemble(const Matrice &ID, const Matrice &GCM,
                  const Matrice &non_bc_sol);
};

#endif // SOLUTION_H
