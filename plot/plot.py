import sys
import numpy as np
from matplotlib.mlab import griddata
import matplotlib.pyplot as plt

t = np.loadtxt(sys.argv[1])
x = t[:,0]
y = t[:,1]
z = t[:,2]

N = 512j
extent = (min(x), max(x), min(y), max(y))

xs,ys = np.mgrid[extent[0]:extent[1]:N, extent[3]:extent[2]:N]

resampled = griddata(x, y, z, xs, ys, interp='linear')

plt.imshow(resampled.T, extent=extent)
#plt.plot(x, y, "r.")
#plt.plot(xs, ys, "b.")
plt.title("solution")
plt.colorbar()
plt.show()
