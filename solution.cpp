#include "solution.h"

#include <fstream>
#include <iostream>

Solution::Solution(const Matrice& ID, const Matrice& GCM,
                   const Matrice& non_bc_sol)
{
    assemble(ID, GCM, non_bc_sol);
}

void Solution::assemble(const Matrice& ID, const Matrice& GCM,
                        const Matrice& non_bc_sol)
{
    int nodes_nb = ID.m();
    solution_ = Matrice(nodes_nb, 3);
    for(int i = 1; i <= nodes_nb; ++i) {
        solution_(i, 1) = GCM(i, 1);
        solution_(i ,2) = GCM(i, 2);
        if(ID(i,1)==0) { // boundary condition node
            solution_(i,3)=ID(i,2);
        } else { // not boundary condition node
            solution_(i,3)=non_bc_sol(ID(i,1));
        }
    }
}

void Solution::to_file(const std::string& sol_file_name) const
{
    std::ofstream sol_file;
    sol_file.open(sol_file_name.c_str());
    if(!sol_file.is_open()) {
        std::cout << "Can not write solution in file." << std::endl;
    };
    sol_file << "#x\ty\tsolution" << std::endl;
    sol_file << solution_;
    sol_file.close();
}
