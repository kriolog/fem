#ifndef DEF_MATRICE
#define DEF_MATRICE

#include <valarray>
#include <cassert>
#include <iosfwd>

using std::size_t;

typedef std::valarray<double> vad;

class Matrice
{
public :
    // Maximal numver of iterations for congGrad, Jacobi and Gaus-Seidel.
    static const size_t _max_iter_nb = 50000;
    // Maximal relative error for congGrad, Jacobi and Gaus-Seidel.
    static const double _rel_error = 1e-6;

    Matrice(size_t m = 1, size_t n = 1);
    Matrice(double t[], size_t m, size_t n);
    Matrice(const vad& va, size_t m, size_t n);
    Matrice(const Matrice& m);


    size_t m() const { return _m; }
    size_t n() const { return _n; }
    Matrice line(size_t i) const {
        assert ((i > 0) && (i <= _m));
        return Matrice(_vad[std::slice(i * _n, _n, 1)], 1, _n);
    }

    Matrice column(size_t j) const {
        assert ((j > 0) && (j <= _n));
        return Matrice(_vad[std::slice(j, _m ,_n)], _m, 1);
    }
    /// Join all lines and devide than resultant line into m lines for create
    /// new matrix.
    Matrice to_matrice(size_t m, size_t n) const {
        assert (_m * _n == m * n);
        return Matrice(_vad, m , n);
    }
    const double& operator()(size_t i, size_t j = 1) const    {
        assert ((i > 0) && (i <= _m));
        assert ((j > 0) && (j <= _n));
        return _vad[(j-1) + (i-1)*_n];
    }
    double& operator()(size_t i, size_t j = 1) {
        return const_cast<double&>(static_cast<const Matrice&>(*this)(i, j));
    }
    Matrice& operator+=(const Matrice& rhs);
    Matrice& operator-=(const Matrice& rhs);
    Matrice& operator*=(const Matrice& rhs);
    // inner product for 2 matrices n * 1 or for 2 matrices 1 * n
    double operator,(const Matrice& m) const {
        assert((_m == 1 && m._m == 1) || (_n == 1 && m._n == 1));
        assert((_m == m._m) && (_n == m._n));
        return (_vad * m._vad).sum();
    }

    double sum() const { return _vad.sum(); }
    Matrice abs() const { return Matrice(std::abs(_vad), _m, _n); }
    double min() const { return _vad.min(); }
    double max() const { return _vad.max(); }
    double norm1() const { return std::abs(_vad).sum(); }
    // racine de la somme des m(i,j)^2
    double norm2() const { return sqrt((_vad*_vad).sum()); }
    double normInf() const { return (std::abs(_vad)).max(); }
    Matrice trans() const;

    friend Matrice operator* (const Matrice& m1, const Matrice& m2);
    friend Matrice operator* (double a, const Matrice& m);

    friend std::ostream& operator<< (std::ostream& flux, const Matrice& m) {
        return m.afficher(flux);
    }

 private:
    vad _vad; //valarray de double contenant les valeurs de la matrice
    size_t _m; //nombre de lignes
    size_t _n; //nombre de colonnes
    //surcharge operator << pour matrice et valarray
    std::ostream& afficher(std::ostream& flux) const;
};

Matrice operator+ (const Matrice& m1, const Matrice& m2);
Matrice operator- (const Matrice& m1, const Matrice& m2);
Matrice operator* (const Matrice& m1, const Matrice& m2);
Matrice operator* (double a, const Matrice& m);
Matrice operator* (const Matrice& m, double a);

/// Gauss solution for linear system A*x=b.
Matrice gauss(const Matrice& A, const Matrice& b);

/**
 *  Implementation of a conjugate gradient method.
 *  Approximate solution of the algebraic linear system A*x=b.
 *  A must be symmetric and positive-definite.
 */
Matrice cong_grad(const Matrice& A, const Matrice& b);

/// Gaus-Seidel iterative method (successive over-relaxation veriation).
Matrice gauss_seidel(const Matrice& A, const Matrice& b, double w = 1);

#endif
