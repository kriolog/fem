#include "assemblage.h"

#include "fe_2d.h"

Assemblage::Assemblage(const Matrice &GCM, const Matrice &IEN,
                       const Matrice &ID, const Matrice &D):
    nel_(IEN.n()),
    nnd_(ID.m()),
    neq_(neq(ID)),
    K_(Matrice(neq_,neq_)),
    M_(Matrice(neq_,neq_)),
    F_(Matrice(neq_,1))
    {
        assemble(GCM,IEN,ID,D);
    }

void Assemblage::assemble(const Matrice& GCM, const Matrice& IEN,
                          const Matrice& ID, const Matrice& D)
{
    for(int el = 1; el <= nel_; ++el) {
        //===========element coordinate matrix and souce/force (rhs) vector====
        Matrice elCoord(4,2);//element coordinate matrix
        Matrice fel(4,1);//element souce (rhs) term verctor
        Matrice mcoef(4,1);//the mass matrix coefficient
        for(int lnn = 1; lnn <= 4; ++lnn) {//lnn:local node number
            int gnn = IEN(lnn,el);//global node number
            elCoord(lnn,1) = GCM(gnn,1);//x
            elCoord(lnn,2) = GCM(gnn,2);//y
            fel(lnn) = ID(gnn,3);//the 3rd column of the ID matrix has the
                                  //source term of each node
            mcoef(lnn) = ID(gnn,4);//the 4th column of the ID matrix has
                                    //mass matrix coeffs.
        }
        Matrice Del = D.line(el).to_matrice(2,2);
        //=========the element matrices===========
        FE2D el2D(elCoord, Del, fel, mcoef);
        Matrice Kel(el2D.Kel()); // 4x4
        Matrice Mel(el2D.Mel()); // 4x4
        Matrice Fel(el2D.Fel()); // 4x1

        //======assembling the global matrices====================
        int ig, jg, ignn, jgnn;// g:global, i,j:two nodes
        for(int il = 1; il <= 4; ++il)// l:local
        {
            ignn = IEN(il,el); //global node number of the 1st node
            ig = ID(ignn,1);//the first column of the ID matrix contains the
                            //node equation numbers

            // It's a boundary node, don't create equation for it.
            if(ig == 0) { continue; }

            F_(ig) += Fel(il); // Update global F matrix
            for(int jl = 1; jl <= 4; ++jl)
            {
                jgnn = IEN(jl,el);//jgnn is a global node number
                jg = ID(jgnn,1);//the ig and jg are equation node numbers

                if(jg != 0) {// unknown element
                    K_(ig,jg) += Kel(il,jl); // Update global K matrix
                    M_(ig,jg) += Mel(il,jl); // Update global M matrix
                } else {// known element (b.c. element)
                    // the 2nd column of the ID matrix has the Direchlet values
                    F_(ig) -= Kel(il,jl)*ID(jgnn,2) + Mel(il,jl)*ID(jgnn,2);
                }
            }
        }
    }
}

int Assemblage::neq(const Matrice& ID)
{
    for(int i = ID.m(); i > 0; --i) {
        // indices of equations increase, for nodes of b.c. they are equal to 0.
        if(ID(i,1) != 0) {
            return ID(i,1);
        }
    }
    return 0;
}
