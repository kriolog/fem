#include "mesh_2d.h"

#include "muParser.h"

#include <fstream>
#include <iostream>
#include <algorithm>
#include <functional>
#include <set>
#include <vector>

using std::size_t;
using std::string;
using std::ifstream;
using std::find_if;
using std::find;
using std::unary_function;
using std::set;
using std::vector;

#include <sstream>
using std::stringstream;

Mesh2D::Mesh2D(const string& geometry_file_name,
               const string& physics_file_name)
{
    try {
        parse_geometry(geometry_file_name);
    } catch(...) {
       std::cout << "Somthing wrong with geometry file parsing." << std::endl;
       throw;
    }
    try {
        parse_physics(physics_file_name);
    } catch(...) {
       std::cout << "Somthing wrong with physical properties file parsing."
                 << std::endl;
       throw;
    }
}

void Mesh2D::parse_geometry(const string& geometry_file_name)
{
    ifstream geometry;
    geometry.open(geometry_file_name.c_str());
    if(!geometry.is_open()) {
        std::cout << "Can not open geometry file." << std::endl;
    };
    string curr_str;
    // Find section $Nodes
    while(getline(geometry, curr_str) && trim(curr_str, false) != "$Nodes") {}
    // Number of nodes.
    size_t nodes_nb;
    geometry >> nodes_nb;
    // Skip the rest of the line.
    getline(geometry, curr_str);
    GCM_ = Matrice(nodes_nb, 2);
    for(size_t i = 1; i <= nodes_nb; ++i) {
    // Take the nodes.
        // Global element number.
        size_t gnn;
        // x, y, (unused) z coords of element.
        double x, y, z;
        geometry >> gnn >> x >> y >> z;
        // Skip the rest of the line.
        getline(geometry, curr_str);
        GCM_(gnn, 1) = x;
        GCM_(gnn, 2) = y;
    }
    // Find section $Elements
    while(getline(geometry, curr_str) && trim(curr_str, false) != "$Elements"){}
    // Number of all elements, including boundary lines.
    size_t elements_all_nb;
    geometry >> elements_all_nb;
    // Skip the rest of the line.
    getline(geometry, curr_str);

    // Boundary condition nodes.
    set<size_t> bc_nodes;
    // Elements.
    vector<Matrice> elements;
    elements.reserve(elements_all_nb);
    for(size_t i = 1; i <= elements_all_nb; ++i) {
        size_t unused;
        // Skip GMSH element number.
        geometry >> unused;
        // id of type of the element.
        unsigned short el_type;
        geometry >> el_type;

        // For skipping unused parameters.
        unsigned short skip_nb;
        for(geometry >> skip_nb; skip_nb > 0; --skip_nb) {
            // Skip unused element properties.
            geometry >> unused;
        }

        // Take elements and bc nodes.
        if(el_type == 1) {
        // Boundary condition 2-node line.
            // New boundary nodes.
            size_t n1, n2;
            geometry >> n1 >> n2;
            bc_nodes.insert(n1);
            bc_nodes.insert(n2);
        } else if(el_type == 3) {
        // Element 4-node quadrangle.
            // Global node numbers for this element.
            size_t n1, n2, n3, n4;
            geometry >> n1 >> n2 >> n3 >> n4;
            Matrice element_nodes(4, 1);
            element_nodes(1) = n1;
            element_nodes(2) = n2;
            element_nodes(3) = n3;
            element_nodes(4) = n4;
            elements.push_back(element_nodes);
        // } else {
        // unused type of element. Do nothing
        }
        // Skip the rest of the line.
        getline(geometry, curr_str);
    }
    geometry.close();

    // Fill IEN_
    IEN_ = Matrice(4, elements.size());
    for(size_t en = 1; en <= elements.size(); ++en) {
        // Global node numbers for nodes of the element.
        Matrice gnns =  elements[en - 1];
        for(unsigned short i = 1; i <= 4; ++i) {
            IEN_(i, en) = gnns(i);
        }
    }

    // Number of current equation.
    size_t curr_eq_nb = 1;
    // Fill first column of ID_.
    ID_ = Matrice(nodes_nb, 5);
    for(size_t gnn = 1; gnn <= nodes_nb; ++gnn) {
        // Desire de Modeste.
        ID_(gnn, 5) = gnn;
        if(bc_nodes.find(gnn) != bc_nodes.end()) {
        // Node is of boundary condition.
            ID_(gnn, 1) = 0;
            continue;
        }
        ID_(gnn, 1) = curr_eq_nb;
        ++ curr_eq_nb;
    }
}

void Mesh2D::parse_physics(const string& physics_file_name)
{
    ifstream physical_props;
    physical_props.open(physics_file_name.c_str());
    if(!physical_props.is_open())  {
        std::cout << "Can not open physical properties file." << std::endl;
    };
    // Coordinates of current point
    double x;
    double y;
    // Current string in file
    string curr_str;

    // Skip comments and empty lines.
    while(getline(physical_props, curr_str) && trim(curr_str).empty()) {}
    // Temperature u
    mu::Parser u;
    u.DefineVar("x", &x);
    u.DefineVar("y", &y);
    u.SetExpr(curr_str);
    u.Eval();

    // Skip comments and empty lines.
    while(getline(physical_props, curr_str) && trim(curr_str).empty()) {}
    // Source term f
    mu::Parser f;
    f.DefineVar("x", &x);
    f.DefineVar("y", &y);
    f.SetExpr(curr_str);
    f.Eval();

    // Skip comments and empty lines.
    while(getline(physical_props, curr_str) && trim(curr_str).empty()) {}
    // Mass term c
    mu::Parser c;
    c.DefineVar("x", &x);
    c.DefineVar("y", &y);
    c.SetExpr(curr_str);
    c.Eval();

    // Skip comments and empty lines.
    while(getline(physical_props, curr_str) && trim(curr_str).empty()) {}
    // Conductivity tensor D term d11
    mu::Parser d11;
    d11.DefineVar("x", &x);
    d11.DefineVar("y", &y);
    d11.SetExpr(curr_str);
    d11.Eval();

    // Skip comments and empty lines.
    while(getline(physical_props, curr_str) && trim(curr_str).empty()) {}
    // Conductivity tensor D terms d12
    mu::Parser d12;
    d12.DefineVar("x", &x);
    d12.DefineVar("y", &y);
    d12.SetExpr(curr_str);
    d12.Eval();

    // Skip comments and empty lines.
    while(getline(physical_props, curr_str) && trim(curr_str).empty()) {}
    // Conductivity tensor D term d21
    mu::Parser d21;
    d21.DefineVar("x", &x);
    d21.DefineVar("y", &y);
    d21.SetExpr(curr_str);
    d21.Eval();

    // Skip comments and empty lines.
    while(getline(physical_props, curr_str) && trim(curr_str).empty()) {}
    // Conductivity tensor D term d22
    mu::Parser d22;
    d22.DefineVar("x", &x);
    d22.DefineVar("y", &y);
    d22.SetExpr(curr_str);
    d22.Eval();

    D_ = Matrice(ID_.m(), 4);
    // Fill column 2,3,4 of ID_ and D_
    for(size_t gnn = 1; gnn <= ID_.m(); ++gnn) {
        x = GCM_(gnn, 1);
        y = GCM_(gnn, 2);
        if(ID_(gnn, 1) == 0) {
        // Only for bc nodes.
            ID_(gnn, 2) = u.Eval();
        }
        ID_(gnn, 3) = f.Eval();
        ID_(gnn, 4) = c.Eval();
        D_(gnn, 1) = d11.Eval();
        D_(gnn, 2) = d12.Eval();
        D_(gnn, 3) = d21.Eval();
        D_(gnn, 4) = d22.Eval();
    }
}

struct NotSpaceOrTab: public unary_function<char, bool> {
    bool operator() (char c) const { return c != ' ' && c != '\t'; }
};

string& Mesh2D::trim(string& str, bool with_comments/*=true*/)
{
    // leading spaces and tabulators
    str.erase(str.begin(),
        find_if(str.begin(), str.end(), NotSpaceOrTab()));
    if(with_comments) {
        // tailing comments
        str.erase(find(str.begin(), str.end(), '$'), str.end());
    }
    // tailing spaces and tabulators
//     str.erase((find_if(str.rbegin(), str.rend(), NotSpaceOrTab())).base(),
//         str.end());
    return str;
}
