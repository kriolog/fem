#ifndef MESH_2D_H
#define MESH_2D_H

#include <string>

#include "matrice.h"

class Mesh2D
{
public:
    /// Takes file names of geomety (gmsh file) and physical propesties.
    Mesh2D(const std::string& geometry_file_name,
           const std::string& physics_file_name);
    const Matrice& GCM() const { return GCM_; }
    const Matrice& IEN() const { return IEN_; }
    const Matrice& ID() const { return ID_; }
    const Matrice& D() const { return D_; }
private:
    /// Parse geomety file.
    void parse_geometry(const std::string& geometry_file_name);
    /// Parse file of physical propesties.
    void parse_physics(const std::string& physics_file_name);
    /// Trim whitespaces & tabulators and optionally comments
    static std::string& trim(std::string& str, bool with_comments = true);

    /// IDentification matrix of nodes.
    /// the matrix ID has 5 columns:
    /// 1:equation number for each node (the position in the global K matrix).
    ///     For bc nodes it is equal to 0.
    /// 2:Direchlet boundary conditions on concerned nodes
    /// 3:source-term (rhs) values at each node
    /// 4:function that multiplies the zero derivative (the potential) term
    /// 5:the global node number (numberin of nodes in the mesh)
    ///
    /// The 5th column's purpose is just to help me put the BC and source term
    /// values at the appropriate nodes in the 2nd and 3rd column.
    Matrice ID_;
    /// Indices of Element Nodes matrix:
    /// global_node_number(local_node_number, element_number).
    Matrice IEN_;
    /// Global Coordinate Matrix:
    /// x and y coordinates for each node w.r.t. its global number.
    Matrice GCM_;
    /// Conductivity tensor D for nodes:
    /// terms d11, d12, d21, d22 w.r.t. nodes global numbers.
    Matrice D_;
};

#endif // MESH_2D_H
