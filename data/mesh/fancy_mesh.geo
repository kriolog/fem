/* Script for generating a mesh over half of an anulus.
 */

/*!!!!!!!!!!!!!!!
 *
 * CAUTION: in order to use this file in gmsh several paramers must
 * be set:
 *  r -- inner radius
 *  R -- outer radius
 *  nradial -- number of divisions in radial direction
 *  nperim  -- number of divisions of the perimeter
 *  structured -- 1 if mesh is tructured, 0 otherwise
 *  elemtype -- elements type
 *                0 -- triangle
 *                1 -- quads
 *                2 -- mixed mesh triangles/quads
 *
 *!!!!!!!!!!!!!!
*/

r = 200;
R = 400;
nradial = 10;
nperim = 10;
structured = 0;
elemtype = 1;

General.Terminal = 1;
Mesh.CharacteristicLengthFromCurvature = 1;

/* Here are the settings for visualisation so I do not have to 
 * enter them each time I work with this file in GUI. An option
 * would be to save the settings in a separate options file.
 */
General.BackgroundGradient = 0;
Mesh.ColorCarousel = 2;
Mesh.SurfaceEdges = 0;
Mesh.SurfaceFaces = 1;
Mesh.Color.One = {78,159,0};
Mesh.Color.Two = {255,29,85};
Mesh.Color.Three = {37,92,255};
Mesh.Color.Four = {182,171,230};
Mesh.Color.Five = {0,255,20};
Mesh.Explode = 0.8;
Mesh.Light = 0;
Mesh.Lines = 1;
Mesh.LineWidth = 3;
/* End of visualisation setup */




stp = newp;
lc = (R-r)/nradial;
Point(stp) = {0, 0, 0}; // Anulus center
Point(newp)  = {r,  0,    0, lc};    // 1 
Point(newp) = {R,  0,    0, lc};    // 2
Point(newp) = {0,  R,    0, lc};    // 3
Point(newp) = {-R, 0,    0, lc};    // 4
Point(newp) = {-r, 0,    0, lc};    // 5
Point(newp) = {0,  r,    0, lc};    // 6

outl[] = {};
innerLin[] = {};
outerLin[]= {};
symmLin[] = {};
vertLin[] = {};

ln1 = newl;
Line(ln1) = {stp+1, stp+2};
outl[] += {ln1};
symmLin[] += {ln1};

ln2 = newl;
Circle(ln2) = {stp+2, stp, stp+3};
outl[] += {ln2};
outerLin[] += {ln2};

ln3 = newl;
Circle(ln3) = {stp+3, stp, stp+4};
outl[] += {ln3};
outerLin[] += {ln3};

ln4 = newl;
Line(ln4) = {stp+4, stp+5};
outl[] += {ln4};
symmLin[] += {ln4};

ln5 = newl;
Circle(ln5) = {stp+5, stp, stp+6};
outl[] += {ln5};
innerLin[] += {ln5};

ln6 = newl;
Circle(ln6) = {stp+6, stp, stp+1};
outl[] += {ln6};
innerLin[] += {ln6};

ln7 = newl;
Line(ln7) = {stp+3, stp+6};
vertLin[] += {ln7};

outline = newll;
Line Loop(outline) = {outl[]};

s1outl = newll;
Line Loop(s1outl) = {ln1, ln2, ln7, ln6};

s2outl = newll;
Line Loop(s2outl) = {ln3, ln4, ln5, -ln7};

sur1 = news;
Plane Surface(sur1) = {s1outl};
sur2 = news;
Plane Surface(sur2) = {s2outl};

Physical Line("leftSym")= {ln1};
Physical Line("rightSym")= {ln4};
Physical Line("innerPerim")= {innerLin[]};
Physical Line("outerPerim")= {outerLin[]};
Physical Line("vertSym") = {ln7};
Physical Surface("all") = {sur1, sur2};

If (structured == 1) 
Transfinite Line{ln1, ln7, ln4} = nradial+1;
Transfinite Line{ln2, ln3, ln5, ln6} = nperim;
Transfinite Surface {sur1}; 
Transfinite Surface {sur2}; 
EndIf 

If (elemtype > 0)
Recombine Surface{sur1, sur2};
  If (elemtype == 1 && structured != 1 ) 
  Mesh.SubdivisionAlgorithm=1;
  EndIf
EndIf

Mesh 2;
Delete {
  Line{7};
}
Delete {
  Line{7};
}
Delete {
  Line{7};
}
Delete {
  Line{7};
}
Delete {
  Line{7};
}
Delete {
  Line{7};
}
Delete {
  Line{6};
}
Delete {
  Line{6};
}
Delete {
  Line{6};
}
Delete {
  Line{6};
}
Delete {
  Line{6};
}
Delete {
  Line{6};
}
Delete {
  Line{6};
}
Delete {
  Line{6};
}
Delete {
  Line{6};
}
Delete {
  Line{6};
}
Delete {
  Point{1};
}
Delete {
  Point{1};
}
Delete {
  Point{1};
}
Delete {
  Point{1};
}
Delete {
  Point{1};
}
Delete {
  Point{1};
}
Delete {
  Point{1};
}
Delete {
  Point{1};
}
