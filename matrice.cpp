#include <iostream>
#include <cassert>
#include "matrice.h"

using std::ostream;
using std::slice;

//Constructeurs
Matrice::Matrice(size_t m, size_t n): _m(m), _n(n), _vad(0.0, m*n)
{}
Matrice::Matrice(double t[], size_t m, size_t n): _m(m), _n(n), _vad(t, m*n)
{}
Matrice::Matrice(const vad& va, size_t m, size_t n): _m(m), _n(n), _vad(va)
{}
Matrice::Matrice(const Matrice& m): _m(m._m), _n(m._n), _vad(m._vad)
{}

Matrice& Matrice::operator+=(const Matrice& rhs)
{
    assert (_m == rhs._m);
    assert (_n == rhs._n);
    _vad += rhs._vad;
    return *this;
}

Matrice& Matrice::operator-=(const Matrice& rhs)
{
    assert (_m == rhs._m);
    assert (_n == rhs._n);
    _vad -= rhs._vad;
    return *this;
}

Matrice operator*(const Matrice& m1, const Matrice& m2)
{
    assert (m1._n == m2._m);
    Matrice result(m1._m, m2._n);
    for(size_t i=0; i < m1._m; ++i) {
        for (size_t j=0; j < m2._n; ++j) {
            vad tmp = m1._vad[slice(i * m1._n, m1._n, 1)]
                * m2._vad[slice(j, m2._m, m2._n)];
            result._vad[j + i * m2._n] = tmp.sum();
        }
    }
    return result;
}


Matrice& Matrice::operator*=(const Matrice& rhs)
{
    Matrice tmp(*this);
    *this = tmp * rhs;
    return *this;
}

ostream& Matrice::afficher(ostream& flux) const
{
    for(size_t i = 0; i < _m; ++i) {
        for (size_t j = 0; j < _n - 1; ++j) {
            flux << _vad[j + i * _n] << '\t';
        }
        flux << _vad[_n - 1 + i * _n];
        flux<<std::endl;
    }
    return flux;
}

Matrice operator+(const Matrice& m1, const Matrice& m2)
{
    Matrice result(m1);
    result += m2;
    return result;
}

Matrice operator-(const Matrice& m1, const Matrice& m2)
{
    Matrice result(m1);
    result -= m2;
    return result;
}

Matrice operator*(double a, const Matrice& m)
{
    Matrice result(m);
    result._vad *= a;
    return result;
}

Matrice operator*(const Matrice& m, double a)
{
    return a * m;
}

Matrice Matrice::trans() const
{
    Matrice result = Matrice(_n, _m);
    for(size_t i = 0; i < _m; ++i) {
        result._vad[slice(i, result._m ,result._n)]
            = _vad[slice(i * _n, result._n, 1)];
    }
    return result;
}

Matrice cong_grad(const Matrice& A, const Matrice& b)
{
    assert(A.m() == A.n());
    assert(A.m() == b.m());
    assert(b.n() == 1);

    size_t n = A.m();

    #ifndef NDEBUG
    for(size_t i = 1; i <= n; ++i) {
        for(size_t j = 1; j < i; ++j) {
            assert(A(i,j) == A(j,i));
        }
    }
    #endif // NDEBUG

    Matrice x = Matrice(n, 1);
    Matrice r = b - A*x;
    Matrice p = r;

    double b_norm2 = b.norm2();
    size_t iter = 0;
    while((b - A*x).norm2() > b_norm2 * Matrice::_rel_error
          && (iter < Matrice::_max_iter_nb)) {
        double rr = (r,r);
        double alpha = rr / (A*p,p);
        x += alpha * p;
        r -= alpha * A * p;
        double beta = (r,r) / rr;
        p = r + beta * p;
        ++iter;
    }
    if(iter >= Matrice::_max_iter_nb) {
        std::cout << "Stop iteration proccess: not convergent." << std::endl;
    } else {
        std::cout << "SLE was successfully solved at "<< iter << " iteration"
                  << std::endl;
    }
    return x;
}

Matrice gauss_seidel(const Matrice& A, const Matrice& b, double w/* = 1*/)
{
    assert(A.m() == A.n());
    assert(A.m() == b.m());
    assert(b.n() == 1);

    const size_t m = b.m();

    Matrice x = Matrice(m, 1);

    double b_norm2 = b.norm2();
    size_t iter = 0;
    while((b - A*x).norm2() > b_norm2 * Matrice::_rel_error
          && (iter < Matrice::_max_iter_nb)) {
        for(size_t i = 1; i <= m; ++i) {
            double Sum = 0;
            for(size_t j = 1; j < i; ++j) {
                // new x values
                Sum += A(i,j) * x(j);
            }
            for(size_t j = i + 1; j <= m; ++j) {
                // old x values
                Sum += A(i,j) * x(j);
            }
            x(i) = (1 - w) * x(i) + (b(i) - Sum) * w / A(i,i);
        }
        ++iter;
    }
    if(iter >= Matrice::_max_iter_nb) {
        std::cout << "Stop iteration proccess: not convergent." << std::endl;
    } else {
        std::cout << "SLE was successfully solved at "<< iter << " iteration"
                  << std::endl;
    }
    return x;
}

Matrice gauss(const Matrice& A, const Matrice& b)
{
    assert(A.m() == A.n());
    assert(A.m() == b.m());
    assert(b.n() == 1);

    size_t n = A.m();
    Matrice A_ = A;
    Matrice b_ = b;
    double g_ik;
    for(size_t k = 1; k <= n - 1; ++k) {
        for(size_t i = k + 1; i <= n; ++i) {
            g_ik = A_(i,k)/A_(k,k);
            for(size_t j = 1; j <= n; ++j) {
                if(j <= k)
                    A_(i,j) = 0;
                else
                    A_(i,j) -= g_ik*A_(k,j);
            }
            b_(i) -= g_ik*b_(k);
        }
    }

    Matrice x_(n, 1);
    x_(n) = b_(n)/A_(n,n);
    for(size_t i = n - 1; i >= 1; --i) {
        double sum = 0;
        for(size_t j = i + 1; j <= n ; ++j)
            sum += A_(i,j)*x_(j);
        x_(i) = (b_(i) - sum) / A_(i,i);
    }

    return x_;
}
